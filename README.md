# What is Service Oriented Architecture (SOA)?
SOA means taking the services from third parties. These services and communication are been provided you through the internet. SOA allows us to provide a large number of facilities from existing services. SOA is a combination of different services.

For example, if you have an E-commerce website where there is a subsystem where you have to make a payment gateway you can take the services from other companies who are making this payment gateway.

![Architecture](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-245.png)

1. Service Provider:  The service provider provides the services and maintains the services and it is responsible for providing the information. Focus on security or easy availability. 
   
2. Service Consumer: The service consumer can locate the service metadata in the registry and develop the required client components to bind and use the service. They can access multiple services if the service provides multiple services.

# Principles
* **Standardized Service Contract** : A service must have some sort of description which describes what the service is about. This makes it easier for client applications to understand what the service does.

* **Loose Coupling** : Less dependency between web services and the client invoking the web service. So as after a change in web services, it should not affect the client application. 

* **Service Abstraction** : The client should not know what is going in the background. For example, if you are making payment then the client should not know how money is been transfer.

* **Service Reusability** : Once the code for a web service is written it should have the ability work with various application types. 

* **Service Autonomy** : Services should have control over the the code it contains.

* **Service Composability**: Services break big problems into little problems. One should never embed all functionality of an application into one single service.
  

# Advantages of SOA
* Easy build
* Easy maintenance
* High Security
* Quality of code Improved
* Cross Platform.
* Better Performance

# Disadvantages of SOA
* High Bandwidth Server
* High Cost
* High Maintenance


# Reference
[Service Oriented Architecture](https://www.youtube.com/watch?v=_dFJOSR-aFs)

[Geeks For Geeks](https://www.geeksforgeeks.org/service-oriented-architecture/)

[Wikipedia](https://en.wikipedia.org/wiki/Service-oriented_architecture)

[SOA Principles](https://www.guru99.com/soa-principles.html)
